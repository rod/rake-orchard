require 'spec_helper'
require 'orchardpackagebuild'

describe OrchardPackageBuild do
  before :each do
    pending
    @sut = OrchardPackageBuild.new
    @sut.command = "C:\\Projects\\Medicover\\Commons\\Orchard.Web\\bin\\Orchard.exe"
    @sut.package = "Medicover.Bootstrap"
    @sut.package_source_folder = "C:\\Projects\\Medicover\\Commons\\src\\modules\\Medicover.Bootstrap"
    @sut.log_level = :verbose
    @sut.output_folder = "C:\\Projects\\Medicover\\Commons\\build\\artifacts"
  end

  it "should ..." do
    @sut.execute
    puts @sut.system_command
  end

end

describe ContentTypesFile do
  before :each do
    content = <<-eos
      <?xml version="1.0" encoding="utf-8"?><Types xmlns="http://schemas.openxmlformats.org/package/2006/content-types"><Default Extension="rels" ContentType="application/vnd.openxmlformats-package.relationships+xml" /><Default Extension="nuspec" ContentType="application/octet" /><Default Extension="csproj" ContentType="application/octet" /><Default Extension="cs" ContentType="application/octet" /><Default Extension="css" ContentType="application/octet" /><Default Extension="png" ContentType="application/octet" /><Default Extension="txt" ContentType="application/octet" /><Default Extension="js" ContentType="application/octet" /><Default Extension="config" ContentType="application/octet" /><Default Extension="psmdcp" ContentType="application/vnd.openxmlformats-package.core-properties+xml" /></Types>
    eos
    files_to_include = ['/bin/Library.dll', 'bin/Some.dll', 'sdsd/sdds.pdb', 'sdsdsd.csproj']
    @sut = ContentTypesFile.new content
    @sut.update files_to_include
  end

  it "should contain dll conent type" do
    r = ''
    @sut.write r
    r.should include("Extension=\"dll\"")
  end
end
