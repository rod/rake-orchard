require 'albacore/albacoretask'
require 'albacore-orchard/config/orchardpackagepublishconfig'
require 'albacore/support/supportlinux'

class OrchardPackagePublish
  include Albacore::Task
  include Albacore::RunCommand
  include Configuration::OrchardPackagePublish
  include SupportsLinuxEnvironment

  attr_array :packages
  attr_accessor  :apikey,
    :gallery,
    :command

  def initialize(command = "NuGet.exe") # users might have put the NuGet.exe in path
    super()
    update_attributes Albacore.configuration.orchardpackagepublish.to_hash
    @command = command
  end

  def execute
    fail_with_message 'packages must be specified.' if @packages.nil?
    fail_with_message 'gallery must be specified.' if @gallery.nil?
    # don't validate @apikey as required, coz it might have been set in the config file using 'SetApiKey'
    @gallery_feed = @gallery + "/FeedService.svc"
    @packages = [@packages] if @packages.is_a? String

    packages_on_gallery = get_package_list_from_gallery
    failure_message = 'OrchardPackage Publish Failed. See Build Log For Details'
    @packages.each do |item|
      @logger.debug item
      if packages_on_gallery.include? item.pathmap('%f')
        @logger.warn "Package #{item} exists on gallery"
        next
      end
      params = create_publish_params_for_package item
      result = execute_single_command params
      fail_with_message failure_message if !result
    end
  end

  def execute_single_command params
    return run_command "OrchardPackage", params
  end

  def create_publish_params_for_package package
    params = []
    params << "push"
    params << "#{@id}"
    params << "\"#{package}\""
    params << "#{@apikey}" if @apikey
    params << "-Source #{gallery}" unless @gallery.nil?
    merged_params = params.join(' ')
    @logger.debug "Build OrchardPackage publish Command Line: #{merged_params}"
    return merged_params
  end

  def get_package_list_from_gallery
    result = `#{@command} list -Source #{@gallery_feed}`
    result = result.split($/)
    result.map! { |a| a.gsub(' ', '.') + '.nupkg' }
    return result
  end

end
