require 'albacore/albacoretask'
begin
    require 'win32/registry'
rescue LoadError => e
end

class IISExpress
    include Albacore::Task
    include Albacore::RunCommand

    attr_accessor :port, :path, :site, :siteid, :config, :additional_parameters, :noop

    def initialize
        @deploy_package = Dir.pwd
        @noop = false
        super()
        update_attributes Albacore.configuration.iisexpress.to_hash
    end

    def execute
        if(@command.nil?)
            @command = get_iisexpress_path
        end

        cmd_params = []
        cmd_params << "/port:#{@port}" if @port != nil 
        cmd_params << "/path:""#{@path.gsub("/", "\\")}""" if @path != nil
        cmd_params << "/config:""#{@config.gsub("/", "\\")}""" if @config != nil
        cmd_params << "/site:""#{@site.gsub("/", "\\")}""" if @site != nil
        cmd_params << "/siteid:""#{@siteid.gsub("/", "\\")}""" if @siteid != nil

        if(!@additional_parameters.nil?)
            cmd_params << @additional_parameters
        end
        cmd_params << get_whatif

        result = run_command "IISExpress Command", "#{cmd_params.join(" ")}"

        failure_msg = "IISExpress Failed.  See build log for details"
        fail_with_message failure_msg if !result
    end

    def get_iisexpress_path
        #check the environment paths
        ENV['PATH'].split(';').each do |path|
            iisexpress_path = File.join(path, 'iisexpress.exe')
            return iisexpress_path if File.exists?(iisexpress_path)
        end

        #check the environment variables
        if ENV['IISExpressPath']
            iisexpress_path = File.join(ENV['IISExpressPath'], 'iisexpress.exe')
            return iisexpress_path if File.exists?(iisexpress_path)
        end

        #check if it's in registry 8.0 version
		if key_exists?('SOFTWARE\Microsoft\IISExpress\8.0')
			Win32::Registry::HKEY_LOCAL_MACHINE.open('SOFTWARE\Microsoft\IISExpress\8.0') do |reg|
				reg_typ, reg_val = reg.read('InstallPath') # no checking for x86 here.
				iisexpress_path = reg_val + 'iisexpress.exe'
				return iisexpress_path if File.exists?(iisexpress_path)
			end
		end
		
		#check if it's in registry 10.0 version
		if key_exists?('SOFTWARE\Microsoft\IISExpress\10.0')
			Win32::Registry::HKEY_LOCAL_MACHINE.open('SOFTWARE\Microsoft\IISExpress\10.0') do |reg|
				reg_typ, reg_val = reg.read('InstallPath') # no checking for x86 here.
				iisexpress_path = reg_val + 'iisexpress.exe'
				return iisexpress_path if File.exists?(iisexpress_path)
			end
		end

        fail_with_message 'IISExpress could not be found is it installed?'
    end
	
	def key_exists?(path)
		begin
			Win32::Registry::HKEY_LOCAL_MACHINE.open(path, ::Win32::Registry::KEY_READ)
			return true
		rescue
			return false
		end
	end

    def get_whatif
        if(@noop)
            return "-whatif"
        end
    end
end
