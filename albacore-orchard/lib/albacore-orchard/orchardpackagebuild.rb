require 'albacore/albacoretask'
require 'albacore-orchard/config/orchardpackagebuildconfig'
require 'albacore/support/supportlinux'
require 'yaml'
require 'zip/zip'
require 'zip/zipfilesystem'
require 'rexml/document'

class ContentTypesFile
  def initialize data
    @xml = REXML::Document.new data
    @xml.context[:attribute_quote] = :double_quote
  end

  def write output
    return @xml.write output
  end

  def update files
    root = @xml.root
    files_ext = files.map { |x| x.pathmap('%{[.],}x') }.uniq
    defined_ext = REXML::XPath.match(@xml, '/*/Default/attribute::Extension').map { |x| x.to_s() }.uniq
    ext_to_put = files_ext - defined_ext
    ext_to_put.each do |x|
      el = root.add_element 'Default'
      el.add_attributes({'ContentType'=>'application/octet', 'Extension'=>x})
    end
  end
end

class OrchardPackageBuild
  TaskName = :orchardpackagebuild
  include Albacore::Task
  include Configuration::OrchardPackageBuild
  include SupportsLinuxEnvironment

  attr_accessor :package,
    :package_source_folder,
    :filename_with_includes,
    :output_folder,
    :command,
    :working_directory,
    :package_output_match

  attr_array :parameters 

  def initialize(command = "Orchard.exe") # users might have put the Orchard.exe in path
    super()
    update_attributes Albacore.configuration.orchardpackagebuild.to_hash
    @command = command
    @working_directory = Dir.pwd
    @filename_with_includes = 'Includes.yml' if @filename_with_includes == nil
    @parameters = [] 
    @failure_message = 'Orchard Package Build Failed. See Build Log For Details'
    @package_output_match = '(?<=Package ").*(?=" successfully created)' if @package_output_match == nil
  end

  def execute
    fail_with_message 'package must be specified.' if @package.nil?
    fail_with_message "Orchard executable '#{@command}' doesn't exists" if !File.exists? @command
    fail_with_message 'output folder must be specified.' if @output_folder.nil?

    @orchard_working_directory = File.dirname(@command)

    created_package = create_package_by_orchard
    fail_with_message 'Created package by Orchard is null or does not exists. Check if orchard package command can be run.' if (created_package == nil || !File.exists?(created_package))
    @logger.debug "Orchard created package #{created_package}"
    include_additional_files_to created_package if @package_source_folder != nil
  end

  def include_additional_files_to package
    include_file = File.join @package_source_folder, @filename_with_includes
    return if !File.exists? include_file
    files_to_include = YAML::load(File.open(include_file))
    if !files_to_include.empty?
      package_filename = package.pathmap('%f')
      if package_filename.start_with? 'Orchard.Module'
        module_name = package_filename.match(/(?<=^Orchard[.]Module[.])[A-z|.|-]*(?=[.])/)[0]
        prefix = "Content\\Modules\\#{module_name}"
      elsif package_filename.start_with? 'Orchard.Theme'
        theme_name = package_filename.match(/(?<=^Orchard[.]Theme[.])[A-z|.|-]*(?=[.])/)[0]
        prefix = "Content\\Themes\\#{module_name}"
      else
        raise "Unknown package type #{package_filename}"
      end

      zf = Zip::ZipFile.new(package)

      buffer = Zip::ZipOutputStream.write_buffer do |out|
        zf.entries.each do |e|
          if e.ftype == :directory
            out.put_next_entry(e.name)
          else
            out.put_next_entry(e.name)
            if e.name == "[Content_Types].xml"
              out.write(update_content_types(e.get_input_stream.read, files_to_include))
            else
              out.write e.get_input_stream.read
            end
          end
        end

        files_to_include.each do |f|
          file = File.join @package_source_folder, f
          file = file.gsub '/', '\\'
          # Entry must have unix path separators
          entry_path = "#{prefix}\\#{f}".gsub '\\', '/'

          file_io = open(file, 'rb')
          out.put_next_entry(entry_path)
          out.write file_io.read()
          file_io.close()
        end
      end
      
      zf.close()

      File.open(package, "wb") {|f| f.write(buffer.string) }
      buffer.close()
      
    end
  end

  def update_content_types data, files
    ct = ContentTypesFile.new data
    ct.update files
    out = ''
    ct.write out
    return out
  end

  def create_package_by_orchard
    params = create_build_params_for_package
    output = run_command "OrchardPackageBuild", params
    return nil if !@last_status
    @last_status = false
    return get_created_package_path output
  end

  def create_build_params_for_package
    params = []
    params << ""
    params << "/wd:#{format_path(@orchard_working_directory)}"

    params << "package create"
    params << "#{@package}"
    params << "#{format_path(@output_folder)}"
    merged_params = params.join(' ')
    @logger.debug "Build OrchardPackage publish Command Line: #{merged_params}"
    return merged_params
  end

  def run_command(name="Command Line", parameters=nil)
    begin
      params = Array.new
      params << parameters unless parameters.nil?
      params << @parameters unless (@parameters.nil? || @parameters.length==0)

      cmd = get_command(params)
      @logger.debug "Executing #{name}: #{cmd}"

      Dir.chdir(@working_directory) do
        output =`#{cmd}` ; @last_status = $?.success?
        return output
      end

    rescue Exception => e
      puts "Error While Running Command Line Tool: #{e}"
      raise
    end
  end

  def get_command(params)
    executable = @command
    unless command.nil?
      executable = File.expand_path(@command) if File.exists?(@command)
    end
    cmd = "\"#{executable}\""
    cmd +=" #{params.join(' ')}" if params.length > 0
    cmd
  end 

  def get_created_package_path output
    re = eval '/' + @package_output_match + '/'
    match = output.match(re)
    if match == nil
        raise "Cannot parse created package using /#{@package_output_match}/ regex from output: --> \n #{output}" 
    end
    return match[0]
  end
end
