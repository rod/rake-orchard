require 'ostruct'
require 'albacore/support/openstruct'

module Configuration
  module OrchardPackageBuild
    include Albacore::Configuration

    def self.orchardpackagebuildconfig
      @config ||= OpenStruct.new.extend(OpenStructToHash).extend(OrchardPackageBuild)
    end

    def orchardpackagebuild
      @config ||= OrchardPackageBuild.orchardpackagebuildconfig
      yield(@config) if block_given?
      @config
    end

  end
end
