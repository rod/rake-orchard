require 'ostruct'
require 'albacore/support/openstruct'

module Configuration
  module Unzip2
    include Albacore::Configuration

    def unzip2
      @unzip2config ||= OpenStruct.new.extend(OpenStructToHash)
      yield(@unzipconfig) if block_given?
      @unzip2config
    end
  end
end

