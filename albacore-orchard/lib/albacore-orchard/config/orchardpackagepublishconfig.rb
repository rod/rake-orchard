require 'ostruct'
require 'albacore/support/openstruct'

module Configuration
  module OrchardPackagePublish
    include Albacore::Configuration

    def self.orchardpackagepublishconfig
      @config ||= OpenStruct.new.extend(OpenStructToHash).extend(OrchardPackagePublish)
    end

    def orchardpackagepublish
      @config ||= OrchardPackagePublish.orchardpackagepublishconfig
      yield(@config) if block_given?
      @config
    end

  end
end
