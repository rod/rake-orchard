albacoreorchard_root = File.expand_path(File.dirname(__FILE__))
$: << albacoreorchard_root
$: << File.join(albacoreorchard_root, "albacore-orchard")
$: << File.join(albacoreorchard_root, "albacore-orchard", 'config')

require "albacore-orchard/iisexpress"
require "albacore-orchard/orchardpackagebuild"
require "albacore-orchard/orchardpackagepublish"
require "albacore-orchard/unzip2"
