# Rake based build system for Orchard application and modules.

This is build system for easier Orchard based applications and modules
development.

## Preparing environment for development ##

1. Install ruby 1.9.3.
2. Install bundler: `gem install bundler`.
3. Install required gems: `bundle install` in this directory.
