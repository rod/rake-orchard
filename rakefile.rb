$LOAD_PATH.unshift File.expand_path('..', __FILE__)

require 'rubygems'
require 'bundler/setup'
require 'ostruct'

gem 'albacore'
require 'albacore'
require 'albacore-orchard/lib/albacore-orchard'
require 'pathname'
require 'rake/clean'
require 'yaml'

require 'zip/zip'
require 'zip/zipfilesystem'
include Zip

class NestedOstruct
  def self.new(hash)
    OpenStruct.new(hash.inject({}){|r,p| r[p[0]] = p[1].kind_of?(Hash) ?  NestedOstruct.new(p[1]) : p[1]; r })
  end
end

Albacore.configure do |config|
  config.yaml_config_folder = "#{ROOT}/build/configs/"
end

ProjectConfig = NestedOstruct.new(YAML::load(File.open(File.join(ROOT, "build/configs/settings.yml"))))

dirs = {
  :tools          => File.expand_path("tools"),
  :buildtools     => File.expand_path("tools/build")
}

@is_linux = RUBY_PLATFORM.include? 'linux'

CLEAN.clear()
CLEAN.include(ProjectConfig.dirs.artifacts + '/*')
CLEAN.include(ProjectConfig.dirs.temp + '/*')
CLEAN.exclude('**/.gitignore')

task :default => 'build:project'

def get_path (path)
  return File.join(ROOT, path)
end

def get_orchard_dest (version)
  return get_path(ProjectConfig.dirs.orchard_dests + "/Orchard_#{version}")
end

def find_solution (path)
  solutions = Dir.glob(File.join(path, '*.sln'), File::FNM_CASEFOLD)
  fail "There is no solution file in #{path}" if solutions.length == 0
  fail "There is more than one solution files in #{path}" if solutions.length > 1
  return solutions[0]
end

def format_path(path)
  "\"#{to_OS_format(path)}\""
end

def to_OS_format(input)
  formatted_input = @is_linux ? input : input.gsub("/", "\\")
  formatted_input
end

namespace :build do

  desc "Builds project"
  msbuild :project do |msb|
    msb.targets :Clean, :Build
    msb.solution = find_solution(get_path(ProjectConfig.dirs.src))
    msb.properties = { :configuration => :Release }
  end

  desc "Creates nuget packages"
  task :packages => ['package:modules', 'package:themes']

  desc "Build, package and publish"
  task :complete => ['clean', 'build:project', 'build:packages', 'publish:packages']

end

namespace :package do

  @orchard_bin = get_path(ProjectConfig.dirs.orchard + '\\bin').gsub('/', '\\')
  @artifacts_path = get_path(ProjectConfig.dirs.artifacts).gsub('/', '\\')

  task :modules do
    ProjectConfig.modules.each do |item|
      name = item.split("/").last
      Rake::Task['package:package'].reenable
      Rake::Task['package:package'].invoke(name, get_path(item))
      puts "Module #{item} packed"
    end if ProjectConfig.modules != nil
  end

  task :themes do
    ProjectConfig.themes.each do |item|
      name = item.split("/").last
      Rake::Task['package:package'].reenable
      Rake::Task['package:package'].invoke(name, get_path(item))
      puts "Theme #{item} packed"
    end if ProjectConfig.themes != nil
  end

  orchardpackagebuild :package, [:package, :package_source_folder] do |t, args|
    t.command = @orchard_bin + '\\Orchard.exe'
    t.package = args.package
    t.package_source_folder = args.package_source_folder
    t.output_folder = @artifacts_path
  end

end

namespace :prepare do

  unzip2 :unzip_orchard, [:version] do |unzip, args|
    version = args.version || ProjectConfig.orchard.version
    unzip.destination = get_path(ProjectConfig.dirs.temp)
    unzip.file = get_path(ProjectConfig.dirs.orchard_zips + "/Orchard.Web.#{version}.zip")
  end

  task :copy_orchard , [:version] => [:unzip_orchard] do |t, args|
    version = args.version || ProjectConfig.orchard.version
    source = get_path(ProjectConfig.dirs.temp).gsub("/", "\\")
    destination = get_orchard_dest(version).gsub("/", "\\")
    sh "xcopy #{source}\\Orchard\\*.* #{destination}\\ /Y /E /Q /EXCLUDE:#{File.join(dirs[:buildtools] + "/CopyDependencies.excluded").gsub("/", "\\")}"
  end

  task :copy_orchard_localization, [:version] do |t, args|
    version = args.version || ProjectConfig.orchard.version
    source = get_path(ProjectConfig.dirs.orchard_loc).gsub("/", "\\")
    destination = get_orchard_dest(version).gsub("/", "\\")
    sh "xcopy #{source}\\*.* #{destination}\\ /Y /E /Q /EXCLUDE:#{File.join(dirs[:buildtools] + "/CopyDependencies.excluded").gsub("/", "\\")}"
  end

  task :link_orchard, [:version] do |t, args|
    version = args.version || ProjectConfig.orchard.version
    link = get_path(ProjectConfig.dirs.orchard).gsub("/", "\\");
    target = get_orchard_dest(version).gsub("/", "\\")
    sh "rmdir #{link}" if Dir.exist?(link)
    sh "cmd /C mklink /D #{link} #{target}"
  end

  task :link_project_modules => [:link_orchard] do
    ProjectConfig.modules.each do |item|
      link = get_path(ProjectConfig.dirs.orchard + '\\Modules\\' + item.split("/").last).gsub('/', '\\')
      target = get_path(item).gsub("/", "\\");
      sh "rmdir #{link}" if Dir.exist?(link)
      sh "cmd /C mklink /D #{link} #{target}"
    end if ProjectConfig.modules != nil
  end
  
  task :link_project_configs => [:link_orchard] do
	ProjectConfig.configs.each do |item|
	  link = get_path(ProjectConfig.dirs.orchard + '\\Config\\' + item.split("/").last).gsub('/', '\\')
	  target = get_path(item).gsub("/", "\\");
	  sh "del /F #{link}" if File.exist?(link)
	  sh "cmd /C mklink /H #{link} #{target}"
	  end if ProjectConfig.configs != nil
  end

  task :link_project_themes => [:link_orchard] do
    ProjectConfig.themes.each do |item|
      link = get_path(ProjectConfig.dirs.orchard + '\\Themes\\' + item.split("/").last).gsub('/', '\\')
      target = get_path(item).gsub("/", "\\");
      sh "rmdir #{link}" if Dir.exist?(link)
      sh "cmd /C mklink /D #{link} #{target}"
    end if ProjectConfig.themes != nil
  end

  desc "for orchard [version] - default #{ProjectConfig.orchard.version}"
  task :development, [:version] => [:clean, :copy_orchard, :copy_orchard_localization, :link_project_modules, :link_project_themes, :link_project_configs] do |t, args|
  end

end

namespace :publish do

  @nugetexec = to_OS_format(File.dirname(__FILE__) + '/bin/NuGet-1.2.exe')
  @gallery = ProjectConfig.gallery.source if ProjectConfig.gallery.source
  @apikey = ProjectConfig.gallery.apikey if ProjectConfig.gallery.apikey

  desc "into gallery"
  task :package, [:name] do |t, args|
    name = args.name
    artifacts_path = ProjectConfig.dirs.artifacts
    package = File.join(artifacts_path, name)
    fail "There is no such package #{package}" if !File.exist? package
    Rake::Task['publish:orchardpublish'].invoke(package)
    puts "Published #{package}"
  end

  desc "into gallery"
  task :packages do
    artifacts_path = ProjectConfig.dirs.artifacts
    packages = Dir.glob(artifacts_path +'/*.nupkg')
    Rake::Task['publish:orchardpublish'].invoke(packages)
    puts "Published #{packages.to_s}"
  end

  orchardpackagepublish :orchardpublish, [:packages] do |o, args|
    o.command = @nugetexec
    o.packages = args.packages
    o.gallery = @gallery
    o.apikey = @apikey
  end
end

namespace :start do

  desc "through IISExpress [port] - default #{ProjectConfig.orchard.port}"
  iisexpress :orchard, [:port] do |iis, args|
    iis.port = args.port || ProjectConfig.orchard.port
    iis.path = get_path(ProjectConfig.dirs.orchard)
  end

end

namespace :test do

  desc "Testing setup by #{ProjectConfig.test.recipe}"
  task :setup, [:prefix] do |t, args|
    orchard_app_data = get_path(ProjectConfig.dirs.orchard + '\\App_Data').gsub('/', '\\')
    orchard_bin = get_path(ProjectConfig.dirs.orchard + '\\bin').gsub('/', '\\')
    orchard_sites = File.join(orchard_app_data, '\\Sites')
    orchard_dependencies = File.join(orchard_app_data, '\\Dependencies')

    connection = ProjectConfig.site.connection
    db_provider = 'SQLServer'
    db_provider = 'SqlCe' if connection == nil
    prefix = args.prefix

    sh "rmdir /S /Q #{orchard_sites}" if Dir.exists?(orchard_sites)
    sh "rmdir /S /Q #{orchard_dependencies}" if Dir.exists?(orchard_dependencies)

    FileUtils.cp_r get_path(ProjectConfig.test.recipe), get_path(ProjectConfig.dirs.orchard + '/Modules/Orchard.Setup/Recipes/')

    cmd = "#{orchard_bin}\\Orchard.exe  /wd:#{orchard_bin} setup /SiteName:\"#{ProjectConfig.site.name}\" /AdminUsername:#{ProjectConfig.site.admin_user} /AdminPassword:#{ProjectConfig.site.admin_password} /DatabaseProvider:#{db_provider} /Recipe:Test"
    cmd = cmd + " /DatabaseConnectionString:\"#{connection}\"" if connection
    cmd = cmd + " /DatabaseTablePrefix:#{prefix}" if prefix

    sh cmd
  end

end
